﻿using System;
using System.IO;

namespace IOProgram
{
    class Streamer
    {
        static void Main(string[] args)
        {
            WriteData();
            ReadData();

            // Store the path of the textfile in your system
            string file = @"C:\\IO\\WriteText.txt";

            // To write all of the text to the file
            string text1 = "This is some text.";
            File.WriteAllText(file, text1);

            // To append text to a file
            string text2 = "This is text to be appended";
            File.AppendAllText(file, text2);

            // To display current contents of the file
            Console.WriteLine(File.ReadAllText(file));
            Console.ReadKey();
        }

        public void DoGit()
        {
            Console.WriteLine("HI");
        }

        public static void WriteData()
        {
            // Store the path of the textfile in your system
            string file = @"C:\\IO\\WriteText.txt";

            // To write all of the text to the file
            string text = "This is some text.";
            File.WriteAllText(file, text);

            // To display current contents of the file
            Console.WriteLine(File.ReadAllText(file));
            Console.WriteLine();

            // To write text to file line by line
            string[] textLines1 = { "This is the first line",
                               "This is the second line",
                              "This is the third line" };

            File.WriteAllLines(file, textLines1);

            // To display current contents of the file
            Console.WriteLine(File.ReadAllText(file));

            // To write to a file using StreamWriter
            // Writes line by line
            string[] textLines2 = { "This is the new first line",
                             "This is the new second line" };

            using (StreamWriter writer = new StreamWriter(file))
            {
                foreach (string ln in textLines2)
                {
                    writer.WriteLine(ln);
                }
            }
            // To display current contents of the file
            Console.WriteLine(File.ReadAllText(file));

            Console.ReadKey();
        }
        public static void ReadData()
        {
            // Store the path of the textfile in your system
            string file = @"C:\\IO\\WriteText.txt";

            Console.WriteLine("Reading File using File.ReadAllText()");

            // To read the entire file at once
            if (File.Exists(file))
            {
                // Read all the content in one string
                // and display the string
                string str = File.ReadAllText(file);
                Console.WriteLine(str);
            }
            Console.WriteLine();

            Console.WriteLine("Reading File using File.ReadAllLines()");

            // To read a text file line by line
            if (File.Exists(file))
            {
                // Store each line in array of strings
                string[] lines = File.ReadAllLines(file);

                foreach (string ln in lines)
                    Console.WriteLine(ln);
            }
            Console.WriteLine();

            Console.WriteLine("Reading File using StreamReader");

            // By using StreamReader
            if (File.Exists(file))
            {
                // Reads file line by line
                StreamReader Textfile = new StreamReader(file);
                string line;

                while ((line = Textfile.ReadLine()) != null)
                {
                    Console.WriteLine(line);
                }

                Textfile.Close();

                Console.ReadKey();
            }
            Console.WriteLine();
        }
    }
}

